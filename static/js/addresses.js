$(".address").click(function () {
    $(".address").attr("src", "/static/images/not_default.png")
    $(this).attr("src", "/static/images/default.png")
    $.ajax({
        type: "PUT",
        url: WEB_URL+"set-default-address/",
        headers: {
            "X-CSRFToken": CSRF_TOKEN
        },
        data: {
            'add_id': $(this).attr("alt")
        },
        success: function (response) {

            var status = response.msg;
            console.log(status);

        }
    })
})


$("#addAddressForm").submit(function (evt) {
    evt.preventDefault();
    var form = $(this);
    $.ajax({
        type: "POST",
        url: WEB_URL+"add-new-address/", // be mindful of url names
        headers: {
            "X-CSRFToken": CSRF_TOKEN
        },
        data: form.serialize(),
        success: function (response) {

            var status = response.msg;
            console.log(status);
            location.reload();

        }
    })

})


$(".delete-address").click(function () {
    var c = confirm("Do you want to delete the address?")
    if (c === true) {
        $.ajax({
            type: "DELETE",
            url: WEB_URL+"delete-address/", // be mindful of url names
            headers: {
                "X-CSRFToken": CSRF_TOKEN
            },
            data: {
                "id": $(this).attr("alt")
            },
            success: function (response) {

                var status = response.msg;
                console.log(status);
                location.reload();

            }
        })
    }
})